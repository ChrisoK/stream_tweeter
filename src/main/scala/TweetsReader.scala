//print to console tweets containig words trzaskowski and jaki
//(names of candidates for mayor in Warsaw)
//please put in twitter4j.properties Your access keys from https://apps.twitter.com/


import org.apache.log4j.{Level, Logger}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.twitter._
import org.apache.spark.sql._

object TweetsReader {
  def main(args: Array[String]): Unit = {
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.WARN)
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.WARN)
    Logger.getLogger("kafka").setLevel(Level.WARN)
    Logger.getLogger("spark").setLevel(Level.ERROR)


    val filters = Array[String]("Jaki", "Trzaskowski")
    val spark = SparkSession
      .builder
      .appName("twit")
      .master("local[3]")
      .getOrCreate()

    val ssc = new StreamingContext(spark.sparkContext, Seconds(2))
    ssc.checkpoint("./checkpoints")

    def cleanString(in: String): String = {
      val removeList = List("@", "http", "RT", "\n")
      val x = in.split(" ")
      var y = ""
      for (u <- x)
        if (!removeList.exists(z => u.startsWith(z))) y = y + " " + u
      y.replaceAll("[^\\x20.a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ0-9?:/()]", " ")
        .replaceAll(" +", " ").trim
    }

    val stream = TwitterUtils.createStream(ssc, None, filters)
    val polish = stream.filter { status =>
      val fromWhere = status.getUser.getLang
      val lang = status.getLang
      lang == "pl" //&& (skad == "pl")
    }

    polish.foreachRDD(rdd => {
      rdd.foreach(rec => {
        var text = ""
        if (rec.isRetweet) text = rec.getRetweetedStatus.getText
        else text = rec.getText
        println(rec.getUser.getScreenName + ": " + text)
        println("=======")
      }
      )
    }
    )
    ssc.start()
    ssc.awaitTermination()
  }
}
