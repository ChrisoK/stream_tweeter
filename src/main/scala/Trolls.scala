//read from hive table and find internet trolls ids (tweets duplicated more than 100 times)

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql._
object Trolls {
  def main(args: Array[String]): Unit = {
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.WARN)
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.WARN)
    Logger.getLogger("kafka").setLevel(Level.WARN)
    Logger.getLogger("spark").setLevel(Level.ERROR)

    val filters = Array[String]("jaki", "trzaskowski")
    val spark = SparkSession
      .builder
      .appName("twit")
      .master("local[4]")
      .config("hive.metastore.uris", "thrift://quickstart.cloudera:9083")
      .enableHiveSupport()
      .getOrCreate()

    val twits = spark.table("twitter.twity_jaki_trzaskowski")
    //or via jdbc (impala connector required)
    // val twits = spark.read.jdbc("jdbc:impala://quickstart.cloudera:21050","twitter.twity_jaki_trzaskowski",new java.util.Properties)

    import spark.implicits._
    val spam = twits.groupBy($"md5sum").count.filter($"count" > 100).select($"md5sum".alias("md5spam"))

    val trollsIds = twits.join(spam, twits("md5sum") === spam("md5spam"))
      .select($"id_autor").distinct.collect

  }

}
