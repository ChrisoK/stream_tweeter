//save to hive table twits containig words "trzaskowski" or "jaki"
//(names of candidates for mayor in Warsaw)
//please put in twitter4j.properties Your access keys from https://apps.twitter.com/



import org.apache.log4j.{Level, Logger}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.twitter._
import org.apache.spark.sql._
import java.security.MessageDigest

object TweetsToHiveTable {
  def main(args: Array[String]): Unit = {
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.WARN)
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.WARN)
    Logger.getLogger("kafka").setLevel(Level.WARN)
    Logger.getLogger("spark").setLevel(Level.ERROR)

    val filters = Array[String]("jaki","trzaskowski")
    val spark = SparkSession
      .builder
      .appName("twit")
      .master("local[2]")
      .config("hive.metastore.uris", "thrift://quickstart.cloudera:9083")
      .enableHiveSupport()
      .getOrCreate()

    val ssc = new StreamingContext(spark.sparkContext, Seconds(2))
    ssc.checkpoint("./checkpoints")

    def md5(s: String) = {
      val m = MessageDigest.getInstance("MD5")
      val b = s.getBytes("UTF-8")
      m.update(b, 0, b.length)
      new java.math.BigInteger(1, m.digest()).toString(16)
    }

    def cleanString(in:String):String ={
      val unwanted = List("@","http","RT","\n")
      val x =in.split(" ")
      var y =""
      for (u<-x)
        if (!unwanted.exists(z=>u.startsWith(z))) y=y+" "+u
      y.replaceAll("[^\\x20.a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ0-9?:/()]"," ")
        .trim.replaceAll(" +", " ")
    }

    val stream = TwitterUtils.createStream(ssc, None,filters)
    val polishTweets = stream.filter { status =>
      val fromWhere = status.getUser.getLang
      val lang = status.getLang
      lang == "pl" 
    }
    polishTweets.foreachRDD(rdd=> {
      import spark.implicits._
      val df = rdd.map{rec => {
        var txt = ""
        if (rec.isRetweet) {txt=rec.getRetweetedStatus.getText}
        else {txt=rec.getText}
        val tekst = cleanString(txt)
        (rec.getId, rec.getUser.getId, tekst,md5(tekst))}}
        .toDF("id_tweet","id_autor","tekst","md5sum").na.drop.coalesce(1)
      df.write.mode("append").saveAsTable("twitter.twity_jaki_trzaskowski")
    }
    )
    ssc.start()
    ssc.awaitTermination()
  }
}
